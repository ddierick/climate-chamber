/*
 * This file is part of the sketch ClimateChamber.ino. For licensing details please refer to ClimateChamber.ino.
 *
 * Some small helper functions to format timestamps for SD/LCD and to synchronize execution to the real-time clock
 */


String formatTimeDate(){                                                                    // format date and time
  sprintf(charBuffer, ("%02u/%02u/%u %02u:%02u:%02u"), timeStamp[5], timeStamp[3],
          timeStamp[6], timeStamp[2], timeStamp[1], timeStamp[0]);
  //sprintf(charBuffer, ("%02u-%02u-%u %02u:%02u:%02u"), timeStamp[3], timeStamp[5],        // uncomment and remove lines above for dd-mm-yy timestamp formatting
  //        timeStamp[6], timeStamp[2], timeStamp[1], timeStamp[0]);
  return charBuffer;
}


String formatTime(){                                                                        // format time
  sprintf(charBuffer, ("%02u:%02u:%02u"), timeStamp[2], timeStamp[1], timeStamp[0]);
  return charBuffer;
}


String formatShortTime(byte* shortTime){                                                    // format hour and minute byte to HH:mm for printing/display
  sprintf(charBuffer, ("%02u:%02u"), shortTime[0], shortTime[1]);
  return charBuffer;
}


void syncToRTC(){                           // ISR rising edge on RTC square wave output (runs once every second when attached)
  sleep_disable();                          // make sure the main loop can not put the device to sleep without any interrupt enabled
}


void goToSleep(){                           // routine to put MCU to sleep
  sleep_enable();
  sleep_cpu();
}


ISR (WDT_vect){
  ++ errorCountWDT;                         // increment WDT error counter
}  
