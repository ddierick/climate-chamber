/*
 * This file is part of the sketch ClimateChamber.ino. For licensing details please refer to ClimateChamber.ino.
 *
 * A function to read the profile from card and store it in EEPROM and two functions related to SD data storage. The first function will write a file header 
 * with configuration data at startup, the second function adds a line with data to the file.
 * Functions to read csv data from SD card are based on the example ReadCsv.ino found in the SdFat library
 */


void readTemperatureProfileFromSD(){
  for (int index=0; index<=479; index++){
    float tmpSetpointTemp;
    csvReadFloat(&cardFile,&tmpSetpointTemp,'\r');                                      // read float from file 
    EEPROM.put(index*2, int(tmpSetpointTemp*100.0)); 
  } 
  delay(10);                                                                            // just to make sure EEPROM write can finish 
}


void writeHeaderDatalog(){
  cardFile.print(F("Start time: "));                                                    // print start time
  cardFile.println(formatTimeDate());
  cardFile.print(F("System name: "));                                                   // print system name
  cardFile.println(systemID);
  cardFile.print(F("Hysteresis: "));                                                    // print hysteresis
  cardFile.print(hysteresis,2);
  cardFile.println(" C");
  cardFile.print(F("Cooling delay: "));                                                 // print cooling delay
  cardFile.print(coolingDelay);
  cardFile.println(F(" seconds"));
  cardFile.print(F("Lights ON: "));                                                     // print time lights ON
  cardFile.println(formatShortTime(timeLightOn));
  cardFile.print(F("Lights OFF: "));                                                    // print time lights OFF
  cardFile.println(formatShortTime(timeLightOff));
  cardFile.print("DS1820 ROM =");                                                       // print DS1820 ROM address
  for(int i=0; i<8; i++){                                                               // this is the ROM address of the sensor found at start-up
    cardFile.write(' ');                                                                // when swapping or connecting after startup, no info
    cardFile.print(addrDS1820[i], HEX);                                                 // about the new sensor will be available on file
    }
  cardFile.println();
  cardFile.println(F("TS,Tset,Tmeas,Light"));
  cardFile.println(F("(mm/dd/yy hh:mm:ss),(C),(C),(-)"));
}


void writeLineDatalog(){
  cardFile.print(formatTimeDate());                                                     // formatted timestamp
  cardFile.print(",");
  cardFile.print(setTemp);                                                              // setpoint T
  cardFile.print(",");
  if (errorFlagDS1820==true){cardFile.print("NaN");}                                    // if temperature read errors occurred write NaN
  else{cardFile.print(sumAvgTemp/float(countAvgTemp));}                                 // valid measured average T
  cardFile.print(",");
  if(lightOn==true){cardFile.println("ON");}                                            // status light
  else{cardFile.println("OFF");}  
}


int csvReadText(SdFile* file, char* str, size_t size, char delim){
  char ch;
  int rtn;
  size_t n = 0;
  
  while (true){
    if (!file->available()){                    // check if EOF
      rtn = 0;
      break;
    }
    if (file->read(&ch, 1) != 1){               // if read error
      rtn = -1;
      break;
    }
    if (ch == '\r'){continue;}                  // skip CR character
    if (ch == delim || ch == '\n'){             // if delimiter or new line
      rtn = ch;
      break;
    }
    if ((n+1) >= size){                         // if string to long
      rtn = -2;
      n--;
      break;
    }
    str[n++] = ch;                              // add character to string
  }
  str[n] = '\0';                                // add terminator to string
  return rtn;                                   
}


int csvReadDouble(SdFile* file, double* num, char delim){
  char buf[20];
  char* ptr;
  int rtn = csvReadText(file, buf, sizeof(buf), delim);
  if (rtn < 0){return rtn;}
  *num = strtod(buf, &ptr);
  if (buf == ptr){return -3;}
  while(isspace(*ptr)) ptr++;
  return *ptr == 0 ? rtn : -4;
}


int csvReadFloat(SdFile* file, float* num, char delim){
  double tmp;
  int rtn = csvReadDouble(file, &tmp, delim);
  if (rtn < 0){return rtn;}
  *num = tmp;
  return rtn;
}


void fileTimeStamp (uint16_t* fileDate, uint16_t* fileTime){
  * fileDate = FAT_DATE(timeStamp[6]+2000,timeStamp[5],timeStamp[3]);
  * fileTime = FAT_TIME(timeStamp[2],timeStamp[1],timeStamp[0]);
}

