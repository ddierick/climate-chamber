/*
 * This file is part of the sketch ClimateChamber.ino. For licensing details please refer to ClimateChamber.ino.
 * 
 * Functions to read system configuration from EEPROM, print a configuration menu over serial, 
 * receive new configuration settings from serial port and store in EEPROM 
 */


void getConfigEEPROM(){
  byte eepromByte;
  EEPROM.get(configOffset, eepromByte);                               // hysteresis is float coded as a byte (value*100)
  hysteresis = eepromByte/100.0;
  EEPROM.get(configOffset+1, coolingDelay);
  EEPROM.get(configOffset+2, timeLightOn);
  EEPROM.get(configOffset+4, timeLightOff);
  EEPROM.get(configOffset+6, highLimitTemp);
  EEPROM.get(configOffset+8, lowLimitTemp);
  EEPROM.get(configOffset+10, systemID);
}


void getConfigSerial(){
  while (exitConfig == false){                                        // begin WHILE in config mode
    charLength = Serial.readBytesUntil(10,charBuffer,25);             // read bytes until NL is found, 30 bytes are read or timeout
    while (Serial.available()){Serial.read();}                        // discart any other data in receive buffer
    if (charLength > 0){                                              // begin IF user input available
      switch (charBuffer[0]){                                         // begin SWITCH/CASE menu options
        case ('a'):
          setTimeStamp();
          break;
        case ('b'):
          setHysteresis();
          break;
        case ('c'):
          setCoolingDelay();
          break;
        case ('d'):
          setTimeLightOn();
          break;
        case ('e'):
          setTimeLightOff();
          break;
        case ('f'):
          setHighLimitTemp();
          break;
        case ('g'):
          setLowLimitTemp();
          break;
        case ('h'):
          setSystemID();
          break;
        case ('x'):
          exitConfig = true;
          break;
        default:
          Serial.println (F("Invalid command"));
      }                                                               // end SWITCH/CASE menu options
    }                                                                 // end IF user input avilable
    rtc.getTimestamp(timeStamp);                                      // update time and display SetupScreen
    displaySetupScreen();
  }                                                                   // end WHILE config menu
}


void printConfigMenuToSerial(){
  Serial.println();
  Serial.println(F(" Set time and date                    a mm/dd/yy hh:mm:ss"));             // print configuration menu
  Serial.println(F(" Set hysteresis (0.05 to 2.50 C)      b x.xx"));
  Serial.println(F(" Set cooling delay (0 to 250 sec)     c xxx"));
  Serial.println(F(" Set time light ON (00:00 to 23:59)   d hh:mm"));
  Serial.println(F(" Set time light OFF (00:00 to 23:59)  e hh:mm"));
  Serial.println(F(" Set upper T limit (-100 to 100 C)    f xxx"));
  Serial.println(F(" Set lower T limit (-100 to 100 C)    g xxx"));
  Serial.println(F(" Set system name (8 chars max)        h SystemID"));
  Serial.println(F(" Exit configuration                   x"));
  Serial.println(F(" Terminate commands with NL and CR"));
  Serial.println();
}


void setTimeStamp(){
  timeStamp[5] = (charBuffer[2]-48)*10+(charBuffer[3]-48);                                    // parse month, day, year, hours, minutes and seconds (in that order)
  timeStamp[3] = (charBuffer[5]-48)*10+(charBuffer[6]-48);                                    // no check for validity TS done
  timeStamp[6] = (charBuffer[8]-48)*10+(charBuffer[9]-48);            
  timeStamp[2] = (charBuffer[11]-48)*10+(charBuffer[12]-48);          
  timeStamp[1] = (charBuffer[14]-48)*10+(charBuffer[15]-48);          
  timeStamp[0] = (charBuffer[17]-48)*10+(charBuffer[18]-48);          
  timeStamp[4] = 0;                                                                           // set day of week arbitrarily to zero
  if (timeStamp[6]<0 || timeStamp[6]>99 || timeStamp[5]<1 || timeStamp[5]>12 ||               // basic check if time/date are valid
    timeStamp[3]<1 || timeStamp[3]>31 || timeStamp[2]<0 || timeStamp[2]>23 ||
    timeStamp[1]<0 || timeStamp[2]>59 || timeStamp[0]<0 || timeStamp[0]>59)
    {Serial.println(F("Invalid time/date value"));}
  else{
    rtc.setTimestamp(timeStamp);                                                              // set timestamp in RTC and confirm change to user
    delay(20);
    rtc.getTimestamp(timeStamp); 
    Serial.print(F("Time and date set to "));                   
    Serial.println(formatTimeDate());
  }
}


void setHysteresis(){
  float tmpHysteresis = (charBuffer[2]-48) + (charBuffer[4]-48)/10.0 + (charBuffer[5]-48)/100.0;       // parse hysteresis formatted x.xx and check range
  if (tmpHysteresis < 0.05 || tmpHysteresis > 2.50){Serial.println(F("Invalid hysteresis value"));}
  else{
    hysteresis = tmpHysteresis;
    EEPROM.put(configOffset, byte(hysteresis*100));
    Serial.print(F("Hysteresis set to "));                                                             // confirm change to user
    Serial.print(hysteresis,2);
    Serial.println(F(" deg C"));
  }
}


void setCoolingDelay(){
  int tmpCoolingDelay = (charBuffer[2]-48)*100 + (charBuffer[3]-48)*10 + (charBuffer[4]-48);           // parse coolingDelay formatted xxx and check range
  if (tmpCoolingDelay < 0 || tmpCoolingDelay > 250){Serial.println(F("Invalid delay value"));}
  else{
    coolingDelay = tmpCoolingDelay;
    EEPROM.put(configOffset+1, coolingDelay);                                                        // write CoolingDelay to one byte EEPROM
    Serial.print(F("Minimum delay between cooling events set to "));                                   // confirm change to user
    Serial.print(coolingDelay);
    Serial.println(F(" seconds"));
  }
}

  
void setTimeLightOn(){
  int tmpHour = (charBuffer[2]-48)*10 + (charBuffer[3]-48);                                            // parse hours, minutes and check range 
  int tmpMinutes = (charBuffer[5]-48)*10 + (charBuffer[6]-48);                                         
  if (tmpHour < 0 || tmpHour > 23 || tmpMinutes < 0 || tmpMinutes > 59){Serial.println(F("Invalid time (valid times are 00:00 to 23:59)"));}
  else{
    timeLightOn[0] = tmpHour;
    timeLightOn[1] = tmpMinutes;
    EEPROM.put(configOffset+2, timeLightOn);
    Serial.print(F("Time to switch lights ON set to "));
    Serial.println(formatShortTime(timeLightOn));
  }
}


void setTimeLightOff(){
  int tmpHour = (charBuffer[2]-48)*10 + (charBuffer[3]-48);                                            // parse hours, minutes and check range 
  int tmpMinutes = (charBuffer[5]-48)*10 + (charBuffer[6]-48);                             
  if (tmpHour < 0 || tmpHour > 23 || tmpMinutes < 0 || tmpMinutes > 59){Serial.println(F("Invalid time (valid times are 00:00 to 23:59)"));}
  else{
    timeLightOff[0] = tmpHour;
    timeLightOff[1] = tmpMinutes;
    EEPROM.put(configOffset+4, timeLightOff);                                    
    Serial.print(F("Time to switch lights OFF set to "));
    Serial.println(formatShortTime(timeLightOff));
  }
}

  
void setHighLimitTemp(){
  int tmpLimit;
  if (charBuffer[2] == '-'){tmpLimit = -1*int((charBuffer[3]-48)*100 + (charBuffer[4]-48)*10 + (charBuffer[5]-48));}          // parse temperature limit with sign and check range 
  else{tmpLimit = int((charBuffer[2]-48)*100 + (charBuffer[3]-48)*10 + (charBuffer[4]-48));}
  if (tmpLimit < -100 || tmpLimit > 100){Serial.println(F("Invalid temperature limit"));}
  else{
    highLimitTemp = tmpLimit;
    EEPROM.put(configOffset+6, highLimitTemp);
    Serial.print(F("Upper temperature limit set to "));
    Serial.print(highLimitTemp);
    Serial.println(F(" deg C"));
  }
}


void setLowLimitTemp(){
  int tmpLimit;
  if (charBuffer[2] == '-'){tmpLimit = -1*int((charBuffer[3]-48)*100 + (charBuffer[4]-48)*10 + (charBuffer[5]-48));}          // parse temperature limit with sign and check range
  else{tmpLimit = int((charBuffer[2]-48)*100 + (charBuffer[3]-48)*10 + (charBuffer[4]-48));}
  if (tmpLimit < -100 || tmpLimit > 100){Serial.println(F("Invalid temperature limit"));}
  else{
    lowLimitTemp = tmpLimit;
    EEPROM.put(configOffset+8, lowLimitTemp);
    Serial.print(F("Lower temperature limit set to "));
    Serial.print(lowLimitTemp);
    Serial.println(F(" deg C"));
  }
}


void setSystemID(){
  byte maxLength = min(8,charLength-3);                                 // limit name to 8 characters
  for (byte i=0; i<maxLength; i++){systemID[i] = charBuffer[i+2];}      // read name into variable systemID skipping the leading command char and space                                                       
  systemID[maxLength] = 0x00;                                           // terminate systemID
  EEPROM.put(configOffset+10, systemID); 
  Serial.print(F("New system name is "));
  Serial.println(systemID);
}

