/*
 * This file is part of the sketch ClimateChamber.ino. For licensing details please refer to ClimateChamber.ino.
 *
 * Class declaration for basic class PCF8523 real time clock.
 * Only functions available are clock initialization, set and get timestamp.
 */


class PCF8523{
  
  public: 
                                       
  PCF8523();                                 // constructor
  boolean init();                            // modifies registers from startup defaults to standard battery backup mode and 1Hz SQ output
  boolean getTimestamp( byte* );             // get timestamp elements (sec, min, hour, day, date, month, year)
  boolean setTimestamp( const byte* );       // set timestamp elements
           
  private:
  
  byte deviceAddress = 0x68;                   // device I2C address
  byte byteToBCD(byte);
  byte BCDtoByte(byte);
};


