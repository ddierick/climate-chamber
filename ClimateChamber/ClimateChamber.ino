 
/* 
 * Code for an arduino based variable temperature controlled chamber with lighting control. 
 * See readme.md in the documentation for details.
 *   
 * Copyright 2017 Diego Dierick. 
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 *  
 * Credits:
 *
 * New LiquidCrystal Library by F. Malpartida is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License. CC BY-SA 3.0
 * It is available at https://bitbucket.org/fmalpartida/new-liquidcrystal/wiki/Home
 *
 * The Arduino SdFat Library is licenced under the MIT license. Copyright (C) 2011-2017 Bill Greiman. It is available at https://github.com/greiman
 * Code snippets to read floats from SD card are taken from the examples that come with the SdFat library.
 */  

#include <SdFat.h>                      // SD card library
#include <Wire.h>                       // I2C for RTC and LCD display     
#include <OneWire.h>                    // OneWire for DS1820
#include <EEPROM.h>                     // built-in EEPROM
#include <LiquidCrystal_I2C.h>          // LCD with I2C backpack
#include <avr/interrupt.h>              // interrupts
#include <avr/sleep.h>                  // sleep modes
#include <avr/wdt.h>                    // watchdog
#include "_05_RTC.h"                    // header PCF8523 real-time clock class

const byte pinIntRTC = 2;               // asign pins interrupt RTC, SD chip select and  relay pins
const byte cs = 10;                      
const byte pinCool = 4;                 // when following the wiring diagram in the original publication based on a 2 channel relay board use pinCool = 6 and pinHeat = 7
const byte pinHeat = 5;                 // and omit pinLight ans pinSpare   
const byte pinLight = 6;                 
const byte pinSpare = 7;   

/* system configuration variables, not initiated as they will be retrieved from EEPROM or set by user using the configuration routine
 * after uploading the sketch the system configuration variables retrieved from EEPROM contain garbage values
 * the user will need to enter valid configuration settings after first power up
 */
byte timeStamp[7];                      // array holding sec, min, hour, day, weekday, month, year (in that order)
char systemID[9];                       // system name ( 8 chars max. plus null termination) 
float hysteresis;                       // temperature hysteresis
byte coolingDelay;                      // minimum delay between cooling events
byte timeLightOn[2];                    // time to switch light on/off
byte timeLightOff[2];                   //  
int highLimitTemp;                      // upper/lower alowable temperature 
int lowLimitTemp;                   

/* variables related to configuration routine and in/output (configuration,SD,LCD)
 */
char charBuffer[30];                    // char array for in and output
int charLength = 0;                     // length of received char array in buffer
boolean exitConfig = false;             // flag configuration routine was exited
const int configOffset = 976;          // EEPROM location where config data starts

/* variables related to temperature measurement
 */
byte addrDS1820[8];                     // holds 8 byte address DS18B20
byte dataDS1820[9];                     // holds 9 data bytes from DS18B20
float measTemp = 0;                     // measured and setpoint temperature 
float setTemp = 0;                      
float sumAvgTemp = 0;                   // sum and number of values summed to calculate average measured temperature
int countAvgTemp = 0;                   

/* status variables cooling, heating and light
 */
boolean coolerOn = false;               // status cooling
boolean coolerOnPrevious = false;       // previous CoolerOn status
boolean heaterOn = false;               // status heating
boolean lightOn = false;                // status light
byte timeSinceCooling = 0;              // time since cooling was last active (constrained to 250)

/* error counts
 */
byte errorCountDS1820 = 0;              // number of errors DS1820, temperature limit settings, watch dog timer and SD card
byte errorCountTemp = 0;                 
byte errorCountWDT = 0;
byte errorCountSD =0;                   
boolean errorFlagDS1820 = false;        // indicates a DS1820 error occurred within last one-minute interval, used to write NaN value to card

PCF8523 rtc;                                                          // instantiate object class FCP8523
OneWire microLAN(8);                                                  // initiate OneWire bus on pin 8 (4k7 pull up)                    
LiquidCrystal_I2C i2cLCD(0x3F, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);     // set LCD I2C address and pin order (some use address 0x27)
SdFat sdCard;                                                         // instantiate SD file system
SdFile cardFile;                                                      // file instance alternately used to access config, profile and datalog
  
void setup(){                                                             // begin SETUP
  pinMode(pinIntRTC, INPUT_PULLUP);                                       // interrupt pin uses internal pullup
  pinMode(pinCool, OUTPUT);
  pinMode(pinHeat, OUTPUT);
  pinMode(pinLight, OUTPUT);
  pinMode(pinSpare, OUTPUT);
  pinMode(cs, OUTPUT);                                                    // set up pin 10 as output to avoid that chip can become SPI slave when using SD library     
  
  switchAllRelaysOff();                                                   // switch all relays off
  Wire.begin();                                                           // start I2C bus
  i2cLCD.begin(16,2);                                                     // initialize 16 by 2 LCD  
  createCustomSymbols();
  rtc.init();                                                             // initialize RTC
  getConfigEEPROM();                                                      // retrieve configuration from EEPROM

  /* start serial, show current configuration on LCD
   * if user enters character 'c' run system configuration routine till exited
   */
  Serial.begin(9600);                                                     // start serial
  while (Serial.available()){Serial.read();}                              // discart any data in receive buffer
  
  Serial.println(F("Send 'c' to enter configuration within next 10 seconds and wait for the menu")); 
  
  for (int i=0; i<10; i++){                                               // wait 10 seconds for user input while displaying configuration
    rtc.getTimestamp(timeStamp);                                          
    displaySetupScreen();
    delay(1000);       
  }
  
  if (Serial.available() > 0 && Serial.read() == 'c'){                    // begin IF config menu entered
    while (Serial.available()) {Serial.read();}                           // discart any data in receive buffer (that would be NL and CR)
    printConfigMenuToSerial();                                            // send menu over serial port 
    getConfigSerial();                                                    // updates system configuration till exited                                              
  }                                                                       // end IF config menu entered

  /* determine DS1820 address, display warning if address invalid
   * initiate measurement to have valid data when entering main loop
   */
  microLAN.reset_search();                                                // reset I2C bus and search DS1820 address (we could use microLAN.skip())
  microLAN.search(addrDS1820);                                           
  if (OneWire::crc8(addrDS1820, 7) != addrDS1820[7]){                     // check I2C address and display warning if invalid
    i2cLCD.clear();                                                      
    i2cLCD.print(F("Invalid CRC address DS1820"));                   
    delay(1000);                                                         
  }
  microLAN.reset();                                                       // reset microLAN, select DS1820 and initiate conversion (data will be ready at start-up)
  microLAN.select(addrDS1820);                                           
  microLAN.write(0x44);                                                  

  /* begin SD card, read profile data and write datalog header, halt system if error occurs
   */
  SdFile::dateTimeCallback(fileTimeStamp);                                // function to provide file timestamps see SD tab

  if (!sdCard.begin(cs)){                                                 // if no SD card present halt system
    strcpy(charBuffer,"No SD card found");
    haltSystemWithMessage(charBuffer);
  } 
    
  if(cardFile.open("profile.txt", FILE_READ)){                            // read profile data if file can be opened
    displayReadProfileScreen();
    readTemperatureProfileFromSD();   
    cardFile.close();                                                  
  }
  else{                                                                   // if profile on card can not be opened (e.g. file missing) halt system
    cardFile.close();
    strcpy(charBuffer,"No T profile");
    haltSystemWithMessage(charBuffer);
  }

  cardFile.open("datalog.txt", FILE_WRITE);
  writeHeaderDatalog();
  cardFile.close();
  

  /* get temperature setpoint before entering main loop 
   * set up watchdog timer, interrupt and sleep mode
   */
  rtc.getTimestamp(timeStamp);                                            // read time into TimeStamp
  setTemp = getTemperatureSetpoint();
  
  cli();                                                                  //disable global interrupts
  MCUSR = 0x00;                                                           //clear reset flags status register (WDRF overrides WDE)
  WDTCSR = (1<<WDCE) | (1<<WDE);                                          //WDCE and WDE have to be set to 1 to modify WDTSCR
  WDTCSR = (0<<WDE) | (1<<WDIE) | (1<<WDP2) | (1<<WDP1) | (1<<WDP0);      //enable WDT interrupts at 2sec interval
  wdt_reset();                                                            //reset WDT
  sei();                                                                  //enable global interrupts

  attachInterrupt(0,syncToRTC,RISING);                                    // when all setup is done attach interrupt and set sleep mode 
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);                                    
}                                                                         // end SETUP 


void loop()                                                                      // begin MAIN loop
  {
  rtc.getTimestamp(timeStamp);                                                   // get RTC data
  displayRunScreen();                                                            // update LCD info every second

  if(timeStamp[0]%10==9){                                                        // every 9th second we search DS1820 device and initiate measurement
    microLAN.reset_search();                                                     // a 12 bit measurement takes about 750ms, so data will be ready by next pass through main loop
    microLAN.search(addrDS1820);                                          
    microLAN.reset();                                  
    microLAN.select(addrDS1820);    
    microLAN.write(0x44);                                                 
  }

  if(timeStamp[0]%10==0){                                                        // begin IF runs every 10 seconds
    measTemp = readTemperatureSensor();                                          // get reading DS1820 (returns -999 if CRC error)
    sumAvgTemp = sumAvgTemp + measTemp;                                          // update sum and interval count for averaging
    countAvgTemp++;  

    if (coolerOnPrevious==true && coolerOn==false){timeSinceCooling=0;}          // if cooling switched off reset variable tracking cooling delay
    if (timeSinceCooling < 245){timeSinceCooling = timeSinceCooling + 10;}       // update timeSinceCooling avoiding byte overflow
    coolerOnPrevious = coolerOn;                                                 // update previous status CoolerOn before modyfying it

    controlTemperatureRelays();                                                  
    controlLightRelays(); 

    if (measTemp==-999){                                                         // DS1820 CRC error, update DS1820 error count and flag
      errorCountDS1820++;  
      errorFlagDS1820 = true;                                                      
    } 
    else{if (measTemp>highLimitTemp || measTemp<lowLimitTemp){errorCountTemp++;}}     // if measured temperature valid, check if in range and update out of range temperature error
  }                                                                              // end IF runs every 10 seconds

  if(timeStamp[0]%60==0){                                                        // begin IF runs every minute
    if(sdCard.cardErrorCode()){                                                  // if card error occurred (on previous write attempt) try to reinitialize card and update error count
      cardFile.close();                                                          
      sdCard.begin();                                                            
      errorCountSD++;
    }
    cardFile.open("datalog.txt", FILE_WRITE);
    writeLineDatalog();
    cardFile.close();
    
    setTemp = getTemperatureSetpoint();                                          // get new T setpoint
    errorFlagDS1820 = false;                                                     // clear flag after each datalog write
    sumAvgTemp = 0;                                                              // clear variables to calculate average
    countAvgTemp = 0;     
  }                                                                              // end IF runs every minute

  doErrorCheck();                                                                // check errors and shut down if needed
  wdt_reset();                                                                   // reset watchdog to avoid interrupt
  goToSleep();                                                                   // sleep till RTC interrupt 
}                                                                                // end MAIN loop




  

