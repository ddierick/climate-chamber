/*
 * This file is part of the sketch ClimateChamber.ino. For licensing details please refer to ClimateChamber.ino.
 *
 * Functions to read temperature profile from SD, retrieve profile temperature setpoint from EEPROM, read the temperature sensor, control relays 
 * for temperature and light and functions to check error counts, switch off all relays and halt the system in case of error.
 */


float getTemperatureSetpoint(){
  int tmpSetpointTemp;                                                                    // value is coded as int 
  int index = int((timeStamp[2]*60 + timeStamp[1])/3.0);                                  // this gives an index between 0 and 479 used to look up the set point  
  EEPROM.get(index*2,tmpSetpointTemp);
  return(float(tmpSetpointTemp/100.0));
}

  
float readTemperatureSensor(){
  microLAN.reset();
  microLAN.select(addrDS1820);    
  microLAN.write(0xBE);                                                                   // read 9 bytes from DS1820 scratchpad
  for ( int i = 0; i < 9; i++){                           
    dataDS1820[i] = microLAN.read();
  }
  int rawTemp = (dataDS1820[1] << 8) | dataDS1820[0];                                     // merge MSB and LSB temperature data, for default 12 bit this is all that needs to happen
  if (OneWire::crc8(dataDS1820, 8) != dataDS1820[8]){return -999;}                        // if CRC error return fault value
  else{return (float)rawTemp/16.0;}                                                       // if data valid return it (math differs for DS18B20)
}


void controlTemperatureRelays(){
  coolerOn = false;                                                                       // cooling/heating off by default
  heaterOn = false;
  if (measTemp - setTemp > hysteresis && timeSinceCooling >= coolingDelay){coolerOn = true;}          // if temperature high and minimum delay since last cooling event elapsed
  if (setTemp - measTemp > hysteresis){heaterOn = true;}                                              // if temperature low start heating
  if (measTemp==-999){                                                                                // if DS1820 CRC error switch both off
    coolerOn = false;
    heaterOn = false;
  }
  digitalWrite(pinCool,!coolerOn);                                                        // update status relay cooler (active low)
  digitalWrite(pinHeat,!heaterOn);                                                        // update status relay heater (active low)
}


void controlLightRelays(){
  int decimalTime = timeStamp[2]*100+timeStamp[1];
  int decimalTimeLightOn = timeLightOn[0]*100+timeLightOn[1];
  int decimalTimeLightOff = timeLightOff[0]*100+timeLightOff[1];
  if ((decimalTime >= decimalTimeLightOn) & (decimalTime < decimalTimeLightOff)){ lightOn = true;}         // check if time inbetween timeLightOn and timeLightOff
  else{lightOn = false;}  
  digitalWrite(pinLight,!lightOn);                                                        // update status relay light (active low)
}


void doErrorCheck(){                                                                      // checks for excessive errors and if needed halts the system
  if (errorCountDS1820 >= 5){
    strcpy(charBuffer,"T sensor error");
    haltSystemWithMessage(charBuffer);
  }
  if (errorCountTemp >= 5){
    strcpy(charBuffer,"T out of range");
    haltSystemWithMessage(charBuffer);
  }     
  if (errorCountSD >= 5){
    strcpy(charBuffer,"SD card error");
    haltSystemWithMessage(charBuffer);
  } 
  if (errorCountWDT >= 30){                                                               // SD related functions take a while to time-out and can meanwhile trigger up to 4 or 5 WDT errors                                                                            
    strcpy(charBuffer,"WDT error");                                                       // so we provide a bit of margin here (amounts to ~60 sec total)
    haltSystemWithMessage(charBuffer);
  }
  if ((timeStamp[2] == 0) && (timeStamp[1] == 0) && (timeStamp[0] == 0)){                 // reset error counts at midnight
    errorCountDS1820 = 0;
    errorCountTemp = 0;
    errorCountWDT = 0;
    errorCountSD = 0;
  }
}

  
void haltSystemWithMessage(char* errorMessage){                                           // show error message, switch relays off and halt systems
  i2cLCD.clear();                                                                         
  i2cLCD.print(errorMessage);                                                             
  i2cLCD.setCursor(0,1);                  
  i2cLCD.print(F("System halted"));       
  switchAllRelaysOff();                   
  while(true){;}                          
}

 
void switchAllRelaysOff(){                                                                // switch off all relay pins (active low)
  digitalWrite(pinCool,HIGH);             
  digitalWrite(pinHeat,HIGH);             
  digitalWrite(pinLight,HIGH);            
  digitalWrite(pinSpare,HIGH);            
}
