/*
 * This file is part of the sketch ClimateChamber.ino. For licensing details please refer to ClimateChamber.ino.
 *
 * Functions implementing LCD routines during system setup, temperature profile reading and regular operation 
 */


void createCustomSymbols(){
  const byte customChars[][8] =                     // up to 8 custom characters for LCD
    {
      { 0x0,0x0,0x4,0xe,0x1f,0x0,0x0 },             // arrow up
      { 0x0,0x0,0x1f,0xe,0x4,0x0,0x0 },             // arrow down
      { 0x0,0xe,0x15,0x17,0x11,0xe,0x0 },           // delay cooling
      { 0x1e,0xa,0xa,0xa,0xa,0xa,0xf },             // hysteresis
      { 0x4,0x11,0xe,0x1b,0xe,0x11,0x4 },           // light ON
      { 0x7,0xa,0x14,0x14,0x14,0xa,0x7 },           // light OFF
      { 0x4,0xa,0xa,0xa,0x11,0x1f,0xe },            // low T limit
      { 0x4,0xa,0xe,0xe,0x1f,0x1f,0xe }             // high T limit
    };
  i2cLCD.createChar( 0, (byte *)customChars[0] );                         // create custom characters
  i2cLCD.createChar( 1, (byte *)customChars[1] );
  i2cLCD.createChar( 2, (byte *)customChars[2] );
  i2cLCD.createChar( 3, (byte *)customChars[3] );
  i2cLCD.createChar( 4, (byte *)customChars[4] );
  i2cLCD.createChar( 5, (byte *)customChars[5] );
  i2cLCD.createChar( 6, (byte *)customChars[6] );
  i2cLCD.createChar( 7, (byte *)customChars[7] );
}

  
void displaySetupScreen(){
  i2cLCD.clear();                                                               // systemID and timestamp always show left adjusted
  i2cLCD.print(systemID);                                                       
  i2cLCD.setCursor(0,1);                                                        
  i2cLCD.print(formatTime());                
  
  if((timeStamp[0]/3)%3==0){                                                    // setup screen 1 of 3 (alternating every 3 seconds)                
    i2cLCD.setCursor(9,0);                                                      // shows hysteresis and cooling delay
    i2cLCD.print(char(3));                
    i2cLCD.setCursor(11,0);                
    i2cLCD.print(hysteresis,2);           
    i2cLCD.setCursor(9,1);                
    i2cLCD.print(char(2));                
    i2cLCD.setCursor(11,1);                
    i2cLCD.print(coolingDelay,1);         
  }
  
  if((timeStamp[0]/3)%3==1){                                                    // setup screen 2 of 3
    i2cLCD.setCursor(9,0);                                                      // shows lightOn and lightOff times
    i2cLCD.print(char(4));                                    
    i2cLCD.setCursor(11,0);                
    i2cLCD.print(formatShortTime(timeLightOn));
    i2cLCD.setCursor(9,1);                
    i2cLCD.print(char(5));                                    
    i2cLCD.setCursor(11,1);                
    i2cLCD.print(formatShortTime(timeLightOff));          
  }
   
  if((timeStamp[0]/3)%3==2){                                                    // setup screen 3 of 3                 
    i2cLCD.setCursor(9,0);                                                      // shows high and low T limits
    i2cLCD.print(char(7));                
    i2cLCD.setCursor(11,0);               
    i2cLCD.print(highLimitTemp);           
    i2cLCD.setCursor(9,1);                
    i2cLCD.print(char(6));                
    i2cLCD.setCursor(11,1);                
    i2cLCD.print(lowLimitTemp);         
  }
}


void displayReadProfileScreen(){
  i2cLCD.clear();                                                               // indicate profile is being read
  i2cLCD.print(F("Reading T"));             
  i2cLCD.setCursor(0,1);                    
  i2cLCD.print(F("profile"));               
}


void displayRunScreen(){
  i2cLCD.clear();                                                               // display timestamp and light and coling/heating status on top line
  i2cLCD.print(formatTime());                                                   // display setpoint and measured temperature on lower line
  
  i2cLCD.setCursor(13,0);                       
  if (lightOn==true){i2cLCD.print(char(4));}
  else{i2cLCD.print(char(5));} 
  
  i2cLCD.setCursor(15,0);                     
  if (coolerOn==true){i2cLCD.print(char(1));}
  else if (heaterOn==true){i2cLCD.print(char(0));}
  else{i2cLCD.write(0x2D);}
  
  i2cLCD.setCursor(0,1);                      
  i2cLCD.print(F("Sp "));  
  i2cLCD.print(setTemp,1);                     
  i2cLCD.print(F("  Pv ")); 
  if (measTemp==-999){i2cLCD.print("NaN");}                                     // if faulty temperature returned show NaN
  else{i2cLCD.print(measTemp,1);}
}


