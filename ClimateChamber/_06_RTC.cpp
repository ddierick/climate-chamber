/*
 * This file is part of the sketch ClimateChamber.ino. For licensing details please refer to ClimateChamber.ino.
 *
 * Implementation of RTC class constructor and member functions
 */


#include <Arduino.h>
#include "_05_RTC.h"
#include <Wire.h>                                                                   // library to use I2C for RTC and LCD display     

/*
 * Specify addresses of PCF8523 registers (we follow datasheet naming).
 * Only a few of the available registers are currently used.
 */

const byte Control_1 = 0x00;                                                        // control registers
const byte Control_2 = 0x01;
const byte Control_3 = 0x02;
const byte Seconds = 0x03;                                                          // time and date registers, seconds (b7 is oscillator stopped flag)
const byte Minutes = 0x04;                                                          // 0-59
const byte Hours = 0x05;                                                            // 0-59
const byte Days =  0x06;                                                            // 1-31
const byte Weekdays = 0x07;                                                         // 0-6
const byte Months = 0x08;                                                           // 1-12
const byte Years = 0x09;                                                            // 0-99
const byte Tmr_CLKOUT_ctrl = 0x0F;                                                  // CLOCKOUT and timer registers


PCF8523::PCF8523(){}                                                                // constructor


boolean PCF8523::init(){                                                            // set up the RTC with 1Hz square wave output and battery backup
  Wire.beginTransmission(deviceAddress);                                            // slave address
  Wire.write(Control_1);                                                            // set address pointer to Control_1 register, auto-increments with each read
  Wire.write(0x00);                                                                 // Control_1 data, this is the default 7pF, running, 24h mode, no INTs
  Wire.write(0x00);                                                                 // Control_2 data, this is the default no INTs, WD disabled, countdown disabled
  Wire.write(0x00);                                                                 // Control_3 data, not default, standard battery switchover enabled
  Wire.endTransmission();  
  
  Wire.beginTransmission(deviceAddress);                                            // slave address
  Wire.write(Tmr_CLKOUT_ctrl);                                                      // set address pointer to Tmr_CLKOUT_ctrl register
  Wire.write(0x30);                                                                 // timers disabled, output SQ 1Hz
  Wire.endTransmission(); 
}


boolean PCF8523::getTimestamp(byte* timestamp){                                     // read timestamp elements RTC and pass back (using provided pointer)
  Wire.beginTransmission(deviceAddress);                                            // slave address
  Wire.write(Seconds);                                                              // set address pointer to Seconds, auto-increments with each read
  Wire.endTransmission();                          
  
  if (Wire.requestFrom(deviceAddress,7) == 0){                                      // if no data received from RTC                          
    for (byte i = 0; i <= 6; i++) timestamp[i] = 0xFF;                              // place 0xFF in pointer destination
    return false;                                                                   // return FALSE
  }
  else{
    for (byte i = 0; i <= 6; i++){
      if (i==0) {timestamp[i] = BCDtoByte(Wire.read() & 0x7F);}                     // place received data in pointer destination masking OS bit from seconds value
      else {timestamp[i] = BCDtoByte(Wire.read());}                                  
    }
    return true;                                                                    // return TRUE
  }
}


boolean PCF8523::setTimestamp(const byte* timestamp){                               // set timestamp elements RTC using provided pointer
  Wire.beginTransmission(deviceAddress);                                            // slave address
  Wire.write(Seconds);                                                              // set address pointer to Seconds, the address auto-increments with each read
  for (byte i = 0; i <= 6; i++) Wire.write(byteToBCD(timestamp[i]));                // set time registers Seconds to Years
  
  if (Wire.endTransmission() == 0){return true;}                                    // if Wire.endTransmission successfull
  else{return false;}                                                               // return FALSE
}


byte PCF8523::BCDtoByte(byte numberBCD){                                            // take BCD byte and convert to single byte number
  int intUnits = numberBCD & 0x0F;
  int intTens = (numberBCD >> 4) * 10;
  return byte (intTens + intUnits);
}


byte PCF8523::byteToBCD(byte number){                                               // take 1 or 2 digit number (represented as single byte) and convert it to one byte BCD
  return byte ((number/10) << 4 | number%10);                                       // number/10 gives the tens as it rounds down
}

