# Code for an arduino based climate chamber

## Introduction

This repository contains code for an arduino based temperature controlled chamber with switched lighting. The system will impose a diurnal temperature profile provided by the user and aditionally switch light (or other actuator) on/off at a configurable time. The chamber itself is a modified fridge with flexible heat tape added inside. Switching the fridge compressor (fridge set to minimum temperature) or heat tape allows full control of the temperature inside the unit. This system can provide a relatively cheap alternative to commercial climate chambers for use in science and can easily be adapted for other applications such as product thermal testing or home brewing.

## About the code

The code is organised in a main tab **ClimateChamber.ino** and a number of tabs grouping code performing related tasks. Code for a very basic implementation of the real-time clock class is split over an **RTC.h** and **RTC.cpp** tab. Tab names have a number in front so they show up in the Arduino IDE in a convenient order. Organizing code in tabs helps with code maintanance and improves readability. You find a ZIP archive containing all code in the download section. You will also need to have the [New LiquidCrystal](https://bitbucket.org/fmalpartida/new-liquidcrystal/wiki/Home) and [Sdfat](https://github.com/greiman) libraries installed in order for the code to compile. This code was tested on Arduino IDE 1.6.2.

## Hardware

The actual chamber is a small fridge with heat tape placed inside to allow both cooling and heating. Depending on the temperature measured inside the chamber and the desired temperature heat tape or fridge compressor will be activated. A configurable temperature hysteresis is implemented to create a dead-band and avoid rapidly switching between cooler and heater. In addition a configurable delay avoids switching the fridge compressor repeatedly within the set cooling delay.

The control system is built around an arduino/genuino UNO or compatible clone with datalogging shield (SD and real time clock, [Adafruit PRODUCT ID: 1141](https://www.adafruit.com/product/1141)) stacked on top. Attached are a 4 channel relay board, a 2x16 LCD display with I2C backpack and a DS18S20 temperature sensor monitoring chamber temperature. The system is powered over the USB port or through the power jack. The logger shield and modules mentioned above come in different flavors and code may have to be adapted slightly to work with particular hardware. Below the wiring instructions and diagram.

*__Note:__ hardware described in the original article is based on a 2 channel relay board controlled using pins 6 and 7. When following the original wiring instructions the pin assignments in the sketch need to be changed. This is indicated in the sketch (line 41 and further).*

##### Wiring the shield
* Distribute 5V and GND using part of the prototyping area (to OneWire, I2C LCD, relay board). To do so braid some wire across pads to create a 5V and GND rail with at least 5 available pads.
* Bring out 5V and GND to the ArduCam I2C LCD. Connect SDA and SCL lines to corresponding lines on the I2C LCD.
* Bring out 5V and GND to the DS18S20 temperature sensor. Connect the sensor data line DQ to pin 8 on the shield. Make sure to add a 4k7 pull-up resistor between DQ line and 5V. Attach a 3.5mm stereo jack to the sensor cable and build a matching connector into the enclosure to make a removable temperature sensor if desired.
* Bring out 5V and GND to the relay module. Wire pin 4 to the input controlling relay 1 (cooling), pin 5 to input relay 2 (heating) and pin 6 to input relay 3 (lighting). Pin 7 is connected to input relay 4 but unused.

*__Note:__ the latest version of the Adafruit logging shield already has 5V and GND power rails on the prototyping area.*

##### Wiring the relay module
* The AC earth and neutral wires are wired through to each receptacle
* The AC hot wire is connected to the common contacts of the relays. The normally open contact of each relay is connected to the respective receptacle. Make sure to respect mains AC "polarity" wiring the hot wire to the correct pin of the receptacles. It is strongly recommended to add a suitable fuse to the incoming AC hot wire.

![Diagram](Documentation/Diagram.jpg?raw=true "Diagram")

*__Note:__ The system can be powered via the USB connector (5V) or the DC power jack (recommended 7-12V). Current draw is roughly 50mA for the Arduino UNO while active and 25mA for the backlit display. Each relay activated draws an additional 70mA which means the total current draw can be up to 350mA when all four relays are activated. The USB input can handle up to 500mA, but expect a several 100mV voltage drop along the USB cable. Use a short cable to minimize voltage drop if problems occur. When supplying power via the DC jack an onboard regulator [NCP1117](https://www.onsemi.com/pub/Collateral/NCP1117-D.PDF) creates the 5V supply at up to 1000mA. However, the total allowable power dissipation is likely below 1.5W and the thermal protection built into the regulator may well kick in when working from a high DC input voltage and drawing high currents (e.g 300mA from 12V supply would dissipate 2.1W). If this occurs try lowering the DC jack input voltage or reduce current draw (lower backlight intensity, switch on 2 relays simultaneously at most or use a solid state relay module).*


## Using the system

##### Requirements
You need to have the Arduino IDE installed to compile and upload code to the board. The Arduino IDE terminal window will come in handy when changing the system configuration. See system configuration for details.

##### System configuration
A number of system parameters need to be configured by the user. To do so the user will have to connect to the board (with code already uploaded) using a terminal program such as the **serial monitor** built into the Arduino IDE. Select the correct COM port and board type and open the terminal window. Set the communication speed to 9600 baud and carriage return or NL+CR as line ending. Following power-up or a reset the user has a 10 second window during which the configuration routine can be entered by sending a charachter 'c' to the board. The following menu should show in the terminal window:

    Set time and date                       a mm/dd/yy hh:mm:ss
    Set hysteresis (0.05 to 2.50 C)         b x.xx
    Set cooling delay (0 to 250 sec)        c xxx
    Set time light ON (00:00 to 23:59)      d hh:mm
    Set time light OFF (00:00 to 23:59)     e hh:mm
    Set upper T limit (-100 to 100 C)       f xxx
    Set lower T limit (-100 to 100 C)       g xxx
    Set system name (8 chars max)           h SystemID
    Exit configuration                      x

Change the configuration by sending the command character and parameter value to the board. Make sure to follow the **exact  format** with leading zeroes where needed. The system responds with a confirmation of the change when a valid command was received. This is also reflected in the configuration info shown on the LCD.

##### Profile
The system is set up to repeat a diel temperature profile. This profile is described in 480 steps of three minutes each with the first profile value corresponding to the first 3 minutes after midnight. The system selects the appropriate target temperature from the profile description based on the system clock. The temperature profile description is read from the SD card at start-up and needs to be provided by the user. To do so create a plain text file named **profile.txt** which consists of a single column with 480 values describing the profile steps. Make sure no header or empty lines exist near the top of the profile description. Profile descriptions can easily be created in a spreadsheet program such as Excel using the **Save as** functions and selecting the tab delimited ***.txt** format. If no temperature profile is found on the SD card the system will halt with a corresponding warning. A profile would look like this (omitting what is enclosed in brackets):

    20.15               (value 1, used 00:00:00 to 00:02:59)
    20.36
    20.63
    ...
    20.16               (value 480, used 23:57:00 to 23:59:59)

An example profile is also provided in the documentation. The profile data is copied to non-volatile memory (EEPROM) on the Arduino and could be retrieved from there on a following power-up. However, the code expects to find a profile description on the SD card each power-up which avoids inadvertedly starting a (new) system without a valid profile.

##### Data
Data is stored in a single file called **datalog.txt**. At power-up a header will be written containing system configuration info followed by a line of data at one minute intervals. On subsequent power-ups data will be appended to the file. An example of the header info and a few lines of data is shown below.

    Start time: 09/17/17 22:13:35
    System name: SysName
    Hysteresis: 0.25 C
    Cooling delay: 60 seconds
    Lights ON: 10:20
    Lights OFF: 11:05
    DS1820 ROM = 28 FF 84 C3 61 16 4 4
    TS,Tset,Tmeas,Light
    (mm/dd/yy hh:mm:ss),(C),(C),(-)
    09/17/17 22:14:00,34.49,26.62,OFF
    09/17/17 22:15:00,34.49,26.62,OFF
    09/17/17 22:16:00,34.88,26.62,OFF
    09/17/17 22:17:00,34.88,26.59,OFF
    09/17/17 22:18:00,34.88,26.56,OFF
    ...

##### Card removal
Removing SD cards while files are kept open for writing or data is being written to the card could lead to corruption of the card and loss of data. The system only writes data to the card at the end of each minute and closes the data file almost immediately after. To prevent SD card corruption the user should therefore **NOT** remove the SD card in the seconds leading up to and following the change of the minute. The same is true when powering down the system. The system clock is always shown on the display and should be checked in order to properly remove cards or shut down.

##### Error messages
The system keeps track of different errors that may occur and shuts down the system if needed. All relays are switched off and a warning will appear on the display. This will happen in the following situations:

* temperature profile missing
* temperature sensor does not respond or data is corrupt
* temperatures are outside set limits
* SD card error
* WDT (watchdog timer) error

All errors exept the first have an associated counter. The system will only be halted when five errors of a given type occurred preventing the system to shut down due to glitches. Error counters are reset every midnight.

*__Note:__ the code presented implements several mechanisms (sensor integrety and temperature limit check, WDT) to shut the system down when things do not work as expected. However this approach is not fail proof. Certain failure modes (e.g. failure relay board, wiring damage) could lead to cooling or heating devices being switched on permanently. In the case of the presented environmental chamber with low heater power (~20W) and a small compressor with the original thermostat still in place this is little cause for concern. However when the control system is used to switch high power devices it is strongly recommended to add thermal cut-offs to the design to protect elements from overheating. For all systems it is recommended to add a fuse to the mains input to protect against shorts in any part of the system.*

*__And another note:__ the WDT is used to make sure a code hang-up does not result in an uncontrolled system. For some excellent info on more advanced concepts such as WDT, interrupts and low power systems check out the pages at https://www.gammon.com.au/ created by Nick Gammon.*

## System modifications

##### General
With the right senors and actuators the system can be adjusted to control a variety of processes. Depending on the actuators used it makes sense to use solid state switching elements and implement a PID control algorithm (this code implements a crude on/off control).

##### Time and date formatting
Depending on platform and locale the timestamps logged to the SD card may not be interpreted correctly (e.g. when opening files in a spreadsheet). Code provided formats timestamps US style mm/dd/yy. You can modify the function **formatTimeDate** in the code tab **_07_Miscellaneous.ino** to format timestamps as desired. This will affect formatting on SD files and LCD. If you also wish to change the timestamp formatting of the configuration routine, you will have to change the **setTimeStamp** and **printConfigMenuToSerial** functions in the tab **_01_Configuration**. 

##### Temperature profile limitations
The system as it is set up has two major limitations with respect to the temperature profile that is recreated. One is the limited size of the profile, another is the timing of the profile. 

The limited size of the profile (480 points) follows from the choice to copy the entire profile in the 1kB arduino EEPROM memory at startup which avoids having to look up temperature setpoints from the SD card while running. Temperatures are internally coded as a signed integer equal to the temperature value times 100. This way we can encode values between -320 and 320 degrees C with two digits after the decimal point using only 2 bytes. Storing float numbers would require 4 bytes per value. Depending on the temperature range needed one can adjust the internal coding scheme (e.g. one byte encoding 0.0 to 51.0 degC in 0.2 deg steps) to allow a more detailed profile. If many more datapoints are needed to describe a profile it will be necessary to read the profile description in blocks from the SD card or add external serial EEPROM. 

The code repeats a given 24h temperature profile and synchronizes it with the real time clock. This could be extended to recreate profiles spanning multiple days but would require a profile description specifying dates as well as time of day in some form. This would lead to large profile descriptions and the need to match timestamps in the file to the system time. Alternative one could provide multiple profiles on the card, each covering 24h of the profile. When named logically (e.g. **mmddyyyy.txt**) the system could pick up the file matching todays date at midnight and start running it. 

For many applications there is no need to synchronize excution of the profile to a real time clock. Instead it is enough to recreate a temperature profile once regardless of time (thermal stress tests, reflow soldering, brewing). Depending on the needs this can simplify the system quite a bit and in many cases the SD card and real time clock could be omitted. The microcontroller can handle all timing and the profile can be hard-coded (fixed) or uploaded through the configuration menu (flexible). One important thing to consider is how the system should handle power outages. If the system needs to pick up the profile where it stopped (however long this may have been) the code needs to store an index to the last executed profile step in non-volatile EEPROM memory and implement a way to restart the profile from that point. Alternative the system just restarts at the beginning of the profile when power cycles.

*__Note:__ some profiles (e.g. sinusoidal, piece-wise linear) can be described mathematically requiring minimal memory. The profile values can then be reconstructed at execution time.*

##### Working with SD cards

When SD files are opened for writing inside the main loop the code checks if card errors occurred and attempts to reinitialise the card if this is the case. This in order to check if a card may accidentally have been removed, got damaged or any other failure occurred. This works, but SD card handling could be improved by using the card detect (and write protect) switch built into most SD connectors, enable CRC to catch file write errors and provide a button for safely removing the card. 

## Contributing

This code is likely not going to be maintained very actively, so feel free to fork this repo for further development. However, I would like to hear back if you find coding errors or if the documentation needs improving.

## Licenses 

Copyright 2017 Diego Dierick.
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.

See COPYING.md in the Documentation folder for the full GNU license.

Material in this readme file is released under a [Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/).